import { Injectable } from '@angular/core';
import { Category } from '../../models/category.model';
import { Item } from '../../models/item.model';
import { Store } from '../../models/store.model';

@Injectable({
  providedIn: 'root',
})
export class StoresService {
  stores: Store[] = [
    new Store(
      1,
      'Alkhafeef',
      'https://hsaa.hsobjects.com/h/restaurants/logos/000/002/987/3e7faa8710e06fb66d56d12407cc9d22-small.png',
      'https://hsaa.hsobjects.com/h/restaurants/android_cover_photos/000/002/987/25fe1c149febd9bda1b3adcf3ddc2db4-large.jpg',
      3.11,
      ['Beverages', 'Sandwiches'],
      [
        new Category(1, 'Promotions', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
        new Category(2, 'Combos', [
          new Item(
            1,
            'Spicy Chicken Combo',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Falafel Combo',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Chicken Bbq Combo',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
        new Category(3, 'Grill Meals', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
      ]
    ),
    new Store(
      2,
      'Baskin Robbins',
      'https://hsaa.hsobjects.com/h/restaurants/logos/000/002/987/3e7faa8710e06fb66d56d12407cc9d22-small.png',
      'https://hsaa.hsobjects.com/h/restaurants/android_cover_photos/000/002/987/25fe1c149febd9bda1b3adcf3ddc2db4-large.jpg',
      4.25,
      ['Beverages', 'Bakery', 'Desserts'],
      [
        new Category(1, 'Promotions', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
        new Category(2, 'Combos', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
        new Category(3, 'Grill Meals', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
      ]
    ),
    new Store(
      3,
      'Coffe Alliance',
      'https://hsaa.hsobjects.com/h/restaurants/logos/000/002/987/3e7faa8710e06fb66d56d12407cc9d22-small.png',
      'https://hsaa.hsobjects.com/h/restaurants/android_cover_photos/000/002/987/25fe1c149febd9bda1b3adcf3ddc2db4-large.jpg',
      3.11,
      ['Beverages', 'Sandwiches'],
      [
        new Category(1, 'Promotions', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
        new Category(2, 'Combos', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
        new Category(3, 'Grill Meals', [
          new Item(
            1,
            'Sandwiches Single Offer',
            24,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            2,
            'Sandwiches Double Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
          new Item(
            3,
            'Sandwiches Family Offer',
            100,
            'https://hsaa.hsobjects.com/h/menuitems/images/006/242/844/d8c1b2bdecf84dbec744c9cf90f81e3a-size400.jpg'
          ),
        ]),
      ]
    ),
  ];
  constructor() {}

  getStores(): Store[] {
    return this.stores;
  }

  getStore(id: number): Store {
    return this.stores.find((store) => store.id == id);
  }

  addStore(store: Store) {
    store.id = this.stores[this.stores.length - 1].id + 1;
    this.stores.push(store);
  }
  addItem(storeId: number, categoryId: number, item: Item) {
    let storeIndex = this.stores.findIndex((store) => store.id == storeId);
    let categoryIndex = this.stores[storeIndex].categories.findIndex((category) => category.id == categoryId);
    this.stores[storeIndex].categories[categoryIndex].items.push(item);
  }
}
