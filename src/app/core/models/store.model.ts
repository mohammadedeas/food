import { Category } from './category.model';

export class Store {
  constructor(
    id: number = 0,
    name: string = '',
    image: string = null,
    cover: string = null,
    rating: number = 0,
    servings: string[] = [],
    categories: Category[] = [],
    lat: number = 0,
    lng: number = 0
  ) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.cover = cover;
    this.rating = rating;
    this.servings = servings;
    this.categories = categories;
    this.lat = lat;
    this.lng = lng;
  }
  id: number;
  name: string;
  image: string;
  cover: string;
  rating: number;
  servings: string[];
  categories: Category[];
  lat: number;
  lng: number;
}
