import { Item } from './item.model';
export class Category {
  constructor(id: number = 0, name: string = '', items: Item[] = []) {
    this.id = id;
    this.name = name;
    this.items = items;
  }
  id: number;
  name: string;
  items: Item[];
}
