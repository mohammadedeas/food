export class Item {
  constructor(id: number = 0, title: string = '', price: number = 0, image: string = null, calories: number = 0) {
    this.id = id;
    this.title = title;
    this.price = price;
    this.image = image;
    this.calories = calories;
  }

  id: number;
  title: string;
  price: number;
  image: string;
  calories: number;
}
