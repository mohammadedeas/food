import { StoresService } from './../../../core/services/stores/stores.service';
import { Store } from './../../../core/models/store.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stores-page',
  templateUrl: './stores-page.component.html',
  styleUrls: ['./stores-page.component.scss'],
})
export class StoresPageComponent implements OnInit {
  stores: Store[] = [];
  store: Store = new Store();
  constructor(private storesService: StoresService, private modalService: NgbModal) {}

  ngOnInit(): void {
    this.stores = this.storesService.getStores();
  }

  addStore(content): void {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {},
      (reason) => {}
    );
  }
  saveStore() {
    this.storesService.addStore(this.store);
    this.store = new Store();
    this.modalService.dismissAll();
  }
}
