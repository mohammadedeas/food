import { Item } from './../../../core/models/item.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from './../../../core/models/category.model';
import { StoresService } from './../../../core/services/stores/stores.service';
import { Store } from './../../../core/models/store.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: ['./store-page.component.scss'],
})
export class StorePageComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private storesService: StoresService,
    private modalService: NgbModal
  ) {}
  store: Store;
  activeCategory: Category;
  item: Item = new Item();

  ngOnInit(): void {
    let id: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.store = this.storesService.getStore(id);
    if (this.store.categories.length) {
      this.activeCategory = this.store.categories[0];
    }
  }
  onCategoryChange(category: Category) {
    console.log(category);

    this.activeCategory = category;
  }
  getServings(): string {
    return this.store.servings.join('-');
  }
  addItem(content): void {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {},
      (reason) => {}
    );
  }
  saveItem() {
    // this.storesService.addStore(this.store);
    this.storesService.addItem(this.store.id, this.activeCategory.id, this.item);
    this.item = new Item();
    this.modalService.dismissAll();
  }
}
