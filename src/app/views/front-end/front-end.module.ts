import { FormsModule } from '@angular/forms';
import { ComponentsModule } from './../components/components.module';
import { FrontEndComponent } from './front-end.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StoresPageComponent } from './stores-page/stores-page.component';
import { StorePageComponent } from './store-page/store-page.component';

const routes: Routes = [
  {
    path: '',
    component: FrontEndComponent,
    children: [
      {
        path: '',
        component: HomePageComponent,
      },
      {
        path: 'stores',
        children: [
          {
            path: '',
            component: StoresPageComponent,
          },
          {
            path: ':id',
            component: StorePageComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  declarations: [FrontEndComponent, HomePageComponent, StoresPageComponent, StorePageComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ComponentsModule, FormsModule],
  entryComponents: [HomePageComponent],
})
export class FrontEndModule {}
