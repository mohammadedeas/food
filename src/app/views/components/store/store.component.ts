import { Store } from './../../../core/models/store.model';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
})
export class StoreComponent implements OnInit {
  @Input() store: Store;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  openStorePage() {
    this.router.navigateByUrl(`/stores/${this.store.id}`);
  }
  getServings(): string {
    return this.store.servings.join('-');
  }
}
