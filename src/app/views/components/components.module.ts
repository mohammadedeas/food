import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreComponent } from './store/store.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { AddStoreFormComponent } from './add-store-form/add-store-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [StoreComponent, ItemCardComponent, AddStoreFormComponent],
  imports: [CommonModule, FormsModule],
  exports: [StoreComponent, ItemCardComponent, AddStoreFormComponent],
})
export class ComponentsModule {}
