import { Store } from './../../../core/models/store.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-store-form',
  templateUrl: './add-store-form.component.html',
  styleUrls: ['./add-store-form.component.scss'],
})
export class AddStoreFormComponent implements OnInit {
  constructor() {}
  store: Store = new Store();
  ngOnInit(): void {}

  save() {
    console.log(this.store);
  }
}
